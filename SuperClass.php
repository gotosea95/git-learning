<?php
/**
 * Created by PhpStorm.
 * User: genri
 * Date: 08.05.2018
 * Time: 20:25
 */

class SuperClass
{
    protected $property;

    /**
     * @return mixed
     */
    public function getProperty()
    {
        return $this->property;
    }
}

